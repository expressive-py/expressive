#!/bin/bash -eu

# Copyright 2024 Russell Fordyce
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# set -x  # XXX testing (testing) only

set +o pipefail  # cause tee et al. to propagate errors

# TODO opportunity for some cleanup features

[[ -f /.dockerenv ]] || {   # not operating inside docker (yet)
    # make sure script is run from expected path like `./test/runtests.sh`
    [[ $(basename $(pwd)) =~ ^expressive.* ]] || { echo "must be run from root of repo" >&2 ; exit 1 ; }
    # check and make some meaningful errors when docker is missing or a fresh install
    docker version >/dev/null || {  # stderr is still displayed
        rc=$?
        echo "unable to use docker (exit ${rc})" >&2
        type -p docker >/dev/null || echo "  docker not installed" >&2  # or $rc==127
        getent group docker | grep --silent '\b'$USER'\b' \
        && echo "  if seeing 'permission denied' usermod and force a new login shell or reboot" >&2 \
        || echo "  user($USER) not a member of docker group (usermod + new login shell or reboot)" >&2
        exit 1
    }
    # now build and run container (this script is the entrypoint)
    echo "building container..."
    set +e  # allow collecting the result of failures
    RESULT_BUILD=$(docker build -t expressive-test:latest -f test/Dockerfile . 2>&1 )
    RC=$?
    [[ 0 = $RC ]] || { echo "build failed code=$RC"$'\n'"output:"$'\n'"$RESULT_BUILD" >&2 ; exit 1 ; }
    set -e
    echo "exec into docker context..."
    exec docker run expressive-test:latest
}

echo "now in docker context..."
echo ""

# gather outputs to ./results
mkdir results
RC=0

echo "linting..."
python3 -m flake8 src/ \
    --max-line-length 150 \
    --ignore E221,E128,W391,E241 \
    > results/flake8.log 2>&1 \
    && echo "no linter errors" \
    || { RC=1 ; cat results/flake8.log ; }
echo ""

# build and install package in-container
# FUTURE with multiple versions, only one might build the package for the others
#   or potentially each build should be made available to publish (seems worse)

echo "building package.."
[[ -n "$(ls --almost-all dist 2>/dev/null)" ]] && { echo "non-empty dist dir" >&2 ; exit 1 ; }
python3 -m build \
    > results/build.log 2>&1 \
    && echo "successfully built python package" \
    || { cat results/build.log ; exit 1 ; }
find ./dist -type f | sed 's/^/  /'
echo ""

echo "installing package.."
path_package="$(find dist/expressive-*.whl)"
python3 -m pip install "$path_package" \
    > results/pip-install.log 2>&1 \
    && echo "successfully installed package from $path_package" \
    || { cat results/pip-install.log ; exit 1 ; }
echo ""

# run unit tests under coverage
echo "testing with coverage..."
[[ ! -f .coveragerc ]] && { echo "missing .coveragerc" >&2 ; exit 1 ; }
python3 -m coverage --version >/dev/null || { echo "missing python coverage module" >&2 ; exit 1 ; }
python3 -m coverage run test/test.py || RC=1
echo "coverage report..."
python3 -m coverage report
echo ""

# ensure source version matches latest in README
echo "testing versions match"
v_src=$(grep "version = " src/expressive/version.py | cut -d '"' -f2)
v_readme=$(grep "##### v" README.md | head -n 1 | cut -d 'v' -f2)
if [[ -z "$v_src" ]] || [[ "$v_src" != "$v_readme" ]] ; then
    echo "mismatched version strings"
    RC=1
fi
echo "source: $v_src, README: $v_readme"

# discover dev hacks like `print("foo")  # XXX`
grep -i "xxx" -r /opt/expressive/{src,test} --exclude runtests.sh --text && RC=1

# TODO actually handle warning on exit
exit $RC
